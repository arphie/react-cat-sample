import React, { Component, useState } from 'react'
import Dropdown from "./Dropdown.js";
import Feed from "./Feed.js";
import {Button, Card} from 'react-bootstrap';

function Home() {
    // this component is responsible for the home page

    // handles state sharing from Dropdown to Feeds
    const [selectedBreed, setselectedBreed] = useState('')
    
    return (
        <div className="main-container">
            <Card>
                <Card.Header>Cat Browser | A React Js Test</Card.Header>
                <Card.Body>
                    <Dropdown setselectedBreed={setselectedBreed} />
                    <Feed selectedBreed={selectedBreed} />
                </Card.Body>
            </Card>
        </div>
    )
}

export default Home
