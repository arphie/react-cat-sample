import axios from 'axios';
import React, { useState, useEffect } from 'react'
import { useLocation } from 'react-router-dom';
import {Form, Alert} from 'react-bootstrap';



function Dropdown({setselectedBreed}) {

    // this component is responsible for the dropdown of cat breeds

    // get locations to handle return from the "back" button on the inner pages
    const location = useLocation();

    // initialize values
    
    // data from API
    let [responseData, setResponseData] = useState('');

    // store selected breed from dropdown
    let [useSelectedBreed, setuseSelectedBreed] = useState('');

    // handels arror from API
    let [apiError, setapiError] = useState(false)

    // converts object from api to array
    var breeds = Array.from(responseData);

    // event onChange event on dropdown
    function handleChange(event) {
        setselectedBreed(event.target.value)
        setuseSelectedBreed(event.target.value)
    }

    // on page load, get list of breeds
    useEffect(() => {
        axios({
            "method": "GET",
            "url": "https://api.thecatapi.com/v1/breeds"
        })
        .then((response) => {
            setResponseData(response.data)
        })
        .catch((error) => {
            // console.log(error)
            setapiError(true)
        })
    }, [useSelectedBreed])
    
    return (
        <div className="row">
            <div className="cat-dropdown col-md-3">
                <Form.Label>Breeds</Form.Label>
                <Form.Control as="select" value={(location.catid ? location.catid : useSelectedBreed)} id="cat-breeds" onChange={handleChange}>
                    <option>Select Breed</option>
                    {breeds.map(items => (
                        <option key={ items.id } value={ items.id }>{ items.name }</option>
                    ))}
                </Form.Control>
            </div>
            {(apiError ? <div className="col-md-12"><Alert variant="warning">Apologies but we could not load new cats for you at this time! Miau!</Alert></div> : "")}
        </div>
    )
}

export default Dropdown
