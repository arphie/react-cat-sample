import {Button, Card} from 'react-bootstrap';
import axios from 'axios';
import React, { useState,useEffect } from 'react'
import { useLocation, withRouter } from 'react-router-dom';


function Innerpage({history}) {

    // this component will handle the cat details

    // will get the if from the url
    const location = useLocation();
    let catIdFromUrl = location.pathname.replace('/', '')

    // will store cat information from api
    let [catInfo, setcatInfo] = useState('')

    // will store the cat id from header to prevent infinite loop of useEffect
    let [info, setInfo] = useState('');
    
    // build api source
    let apibuild = "https://api.thecatapi.com/v1/images/"+catIdFromUrl;

    // on load, get cat information
    useEffect(() => {
        if(info !== catIdFromUrl){
            axios({
                "method": "GET",
                "url": apibuild
            })
            .then((response) => {
                setcatInfo(response.data)
            })
            .catch((error) => {
                // console.log(error)
            })
        }
        setInfo(catIdFromUrl)
    }, [info, catIdFromUrl, apibuild])

    // handles "back" button events
    function handleClick(event) {
        history.push({
            pathname: `/`,
            catid: event.target.value
        })
    }

    if(Object.keys(catInfo).length === 0){
        return (
            <div>
                {/* Loading */}
            </div>
        )
    } else {
        return (
            <div>
                <div className="cat-information">
                    <div className="cat-inner-information">
                        <Card>
                            <Card.Header as="h5">
                                <Button variant="primary" onClick={handleClick} value={ catInfo.breeds[0].id }>Back</Button>
                            </Card.Header>
                            <Card.Img variant="top" src={ catInfo.url } />
                            <Card.Body>
                                <Card.Title>{ catInfo.breeds[0].name }</Card.Title>
                                <Card.Text>{ catInfo.breeds[0].origin }</Card.Text>
                                <Card.Text>{ catInfo.breeds[0].temperament }</Card.Text>
                                <Card.Text>{ catInfo.breeds[0].description }</Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
            </div>
        )
    }
    
}

export default withRouter(Innerpage)
