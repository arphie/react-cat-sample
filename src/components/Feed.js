/* eslint-disable */
import React, { useState, useEffect} from 'react'
import axios from 'axios';
import { useLocation, withRouter } from 'react-router-dom';
import {Button, Card, Alert} from 'react-bootstrap';

function Feed({selectedBreed, history}) {

    // this component will load the feeds from the api base on the selected breed on the dropdown

    // get locations to handle return from the "back" button on the inner pages
    const location = useLocation();
    if(location.catid){
        selectedBreed = location.catid;
    }
    
    // initialize variables 

    // can information that will show on the browser
    let [catsInfo, setCatsInfo] = useState(null)

    // the number of times the "load more" button is clicked
    // this will be the bases of the api builder
    let [loadcount, setloadcount] = useState(1)

    // the list of cat id, will be used to check if the items on the cat list is unique
    let [catLists, setcatLists] = useState(null)

    // to make the "load more" button hide and show
    let [showLoad, setshowLoad] = useState(true)

    // handels error message on api issues
    let [apiError, setapiError] = useState(false)

    // api that will be used to request to populate the feed
    let apiBuild = "https://api.thecatapi.com/v1/images/search?page="+loadcount+"&limit=5&breed_id="+selectedBreed;

    // listener for changes in category
    useEffect(() => {
        if(selectedBreed !== ""){
            axios({
                "method": "GET",
                "url": apiBuild
            })
            .then((response) => {
                let stackdata = [];
                let catslistinfo = [];
                response.data.forEach((item, key) => {
                    let cat_item = [];
                    cat_item['url'] = item.url;
                    cat_item['id'] = item.id;
                    catslistinfo.push(cat_item)
                    stackdata.push(item.id)
                });
                setcatLists(stackdata)
                setCatsInfo(catslistinfo)
                setshowLoad(true)
                setapiError(false)
            })
            .catch((error) => {
                // console.log(error)
                setapiError(true)
            })
            setloadcount(1);
        }
    }, [selectedBreed]);

    // listener for changes in "load more" cats
    useEffect(() => {
        if(selectedBreed !== ""){
            axios({
                "method": "GET",
                "url": apiBuild
            })
            .then((response) => {
                response.data.forEach((catitem, key) => {
                    
                    if(catLists.includes(catitem.id)){
                        setshowLoad(false)
                        return
                    }
                    let cat_item = [];
                    cat_item['url'] = catitem.url;
                    cat_item['id'] = catitem.id;
                    catsInfo.push(cat_item)
                    catLists.push(catitem.id)
                });
                setapiError(false)
            })
            .catch((error) => {
                // console.log(error)
                setapiError(true)
            })
        }
    }, [loadcount])

    // converts object from api to array
    let catinfo = (catsInfo !== null ? Array.from(catsInfo) : [])

    // handles "view Details" event
    function handleClick(event) {
        history.push({
            pathname: `/${event.target.value}`,
            catid: event.target.value
        })
    }

    // handles "Load more" event
    function loadMore(event) {
        setloadcount(parseInt(event.target.value) + 1)
    }

    if (Object.keys(catinfo).length === 0) {
        return (
            <div className="no-cat">
                {(apiError ? <Alert variant="warning">Apologies but we could not load new cats for you at this time! Miau!</Alert> : "")}
                No cats available
            </div>
        )
    } else {
        return (
            <div>
                <div className="row">
                    {catinfo.map((cats, key) => (
                        <div className="cat-items col-md-3" data-item={key}  key={key}>
                            <Card>
                                <Card.Img variant="top" src={ cats.url } />
                                <Card.Body>
                                    <Button block variant="primary" value={cats.id} onClick={handleClick}>View Details</Button>
                                </Card.Body>
                            </Card>
                        </div>
                    ))}
                </div>
                {showLoad ? (<div className="load-more"><Button variant="primary" value={loadcount} onClick={loadMore}>Load More</Button></div>) : ""}
                
            </div>
        )
    }
    
    
}


export default withRouter(Feed)
