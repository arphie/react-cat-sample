import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Home from "./components/Home.js";
import Innerpage from "./components/Innerpage.js";
import React, { Component, useState } from 'react'
import { Route, Switch } from 'react-router-dom';

function App() {
    // this is the main component
    
    return (
      <div className="App">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/:catid" component={Innerpage} />
        </Switch>
      </div>
    )
}

export default App;
